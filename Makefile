up:
	cd traefik && docker-compose up -d
	docker-compose up -d

down:
	docker-compose down
	cd traefik && docker-compose down

php-kats:
	docker-compose exec php sh

cache:
	docker-compose exec php php bin/console c:c